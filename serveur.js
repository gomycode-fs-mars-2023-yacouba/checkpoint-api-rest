const express = require('express');
const mongoose = require('mongoose');
require('dotenv').config();

const app = express();


connectDataBase().catch(err => console.log(err));

async function connectDataBase() {
  await mongoose.connect('mongodb://127.0.0.1:27017/utilisateur-db');
  console.log("database is connect")
}
const Utilisateur = require('./models/Utilisateur');

app.use(express.json());

// Récupérer tous les utilisateurs
app.get('/utilisateurs', (req, res) => {
  Utilisateur.find()
    .then((utilisateurs) => {
      res.json(utilisateurs);
    })
    .catch((error) => {
      res.status(500).json({ error: 'Erreur lors de la récupération des utilisateurs' });
    });
});

// Ajouter un nouvel utilisateur à la base de données
app.post('/utilisateurs', (req, res) => {
  const nouvelUtilisateur = new Utilisateur(req.body);
  nouvelUtilisateur.save()
    .then((utilisateur) => {
      res.json(utilisateur);
    })
    .catch((error) => {
      res.status(500).json({ error: 'Erreur lors de l\'ajout de l\'utilisateur' });
    });
});

// Modifier un utilisateur par ID
app.put('/utilisateurs/:id', (req, res) => {
  Utilisateur.findByIdAndUpdate(req.params.id, req.body, { new: true })
    .then((utilisateur) => {
      res.json(utilisateur);
    })
    .catch((error) => {
      res.status(500).json({ error: 'Erreur lors de la modification de l\'utilisateur' });
    });
});

// Supprimer un utilisateur par ID
app.delete('/utilisateurs/:id', (req, res) => {
  Utilisateur.findByIdAndRemove(req.params.id)
    .then(() => {
      res.json({ message: 'Utilisateur supprimé avec succès' });
    })
    .catch((error) => {
      res.status(500).json({ error: 'Erreur lors de la suppression de l\'utilisateur' });
    });
});

module.exports = app;
